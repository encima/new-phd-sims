package com.encima.nodes;

public class SendState {

	private boolean sending;
	private Object file;
	private double transTime;

	public SendState(Object file, double transTime) {
		this.setFile(file);
		this.setTransTime(transTime);
		this.setSending(true);
	}

	boolean isSending() {
		return sending;
	}

	void setSending(boolean sending) {
		this.sending = sending;
		if (sending == false) {
			this.setFile(null);
			this.setTransTime(0);
		}
	}

	Object getFile() {
		return file;
	}

	void setFile(Object file) {
		this.file = file;
	}

	double getTransTime() {
		return transTime;
	}

	void setTransTime(double transTime) {
		this.transTime = transTime;
	}

}