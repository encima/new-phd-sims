package com.encima.nodes;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.Vector;

import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.engine.schedule.ScheduleParameters;
import repast.simphony.engine.schedule.ScheduledMethod;
import repast.simphony.parameter.Parameters;
import repast.simphony.query.space.continuous.ContinuousWithin;
import repast.simphony.space.continuous.ContinuousSpace;
import repast.simphony.space.graph.Network;
import repast.simphony.space.graph.RepastEdge;

import com.encima.dwc.DarwinCore;
import com.encima.dwc.Identification;
import com.encima.dwc.Image;

public class BaseNode {

	private String projection = "K-HAS/sensor network";
	Vector<DarwinCore> interesting, empty, unknown, archives;
	ContinuousSpace<Object> space;
	Network<Object> network;
	String mode;
	String transMethod;
	int nodeID, connectedNodes;
	BaseNode receiver;
	String propsFile = "properties/rf.properties";
	Double transRate, transRange;
	int hopsToDA = -1;
	SendState sendState;
	String routingMode;

	public BaseNode(int nodeID, String transMethod,
			ContinuousSpace<Object> space, Network<Object> net, String mode,
			String tm) {
		this.setNodeID(nodeID);
		this.setTransMethod(transMethod);
		this.setSpace(space);
		this.setNetwork(net);
		this.setMode(mode);
		this.setTransMethod(tm);
		
		Parameters params = RunEnvironment.getInstance().getParameters();
		this.routingMode = (String) params.getValue("routingMode");

		this.empty = new Vector<DarwinCore>();
		this.archives = new Vector<DarwinCore>();
		this.interesting = new Vector<DarwinCore>();
		this.unknown = new Vector<DarwinCore>();
		this.sendState = null;

		if (this.getTransMethod().toLowerCase().equals("rf"))
			propsFile = "properties/rf.properties";
		else if (this.getTransMethod().toLowerCase().equals("zigbee"))
			propsFile = "properties/zigbee.properties";
		else if (this.getTransMethod().toLowerCase().equals("wifi"))
			propsFile = "properties/wifi.properties";

		try {
			Properties transProps = new Properties();
			transProps.load(new FileInputStream(propsFile));
			this.setTransRange(Double.parseDouble(transProps
					.getProperty("range")));
			if (this instanceof SensingNode) {
				transRate = Double.parseDouble(transProps.getProperty("rate"));
				this.setTransRate((double) (20 + (int) (Math.random() * ((transRate - 20) + 1))));
//				this.setTransRate(transRate);
//				System.out.println(this.getNodeID());
			} else {
				this.setTransRate(Double.parseDouble(transProps
						.getProperty("rate")));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void receiveBroadcast(BaseNode requester) {
		this.setReceiver(requester);
		this.setHopsToDA(requester.getHopsToDA() + 1);
		handleEdge(requester);
		Iterator iter = new ContinuousWithin(getSpace(), this,
				this.getTransRange()).query().iterator();
		// int count = 0;
		while (iter.hasNext()) {
			BaseNode obj = (BaseNode) iter.next();
			if ((obj.getHopsToDA() == -1 && obj instanceof SensingNode)
					|| (obj instanceof SensingNode && obj.getHopsToDA() != -1 && obj
							.getHopsToDA() + 1 > this.getHopsToDA() + 1)) {
				obj.receiveBroadcast(this);
				// count++;
			}
		}
	}

	public void handleEdge(BaseNode n) {
		List<RepastEdge> edges = new ArrayList<RepastEdge>();
		Iterator iter = network.getEdges(this).iterator();
		while (iter.hasNext()) {
			edges.add(((RepastEdge) iter.next()));
		}

		for (RepastEdge edge : edges) {
			network.removeEdge(edge);
		}
		RepastEdge re = new RepastEdge(this, n, false, 2);
		network.addEdge(re);
	}

//	@ScheduledMethod(start = 10, interval = 1, priority = 1.7976931348623157E308d)
	public void send() {
		if (this instanceof CentralNode) {
			// System.out.println(this.getSendState());
			return;
		}
//		 System.out.println(this);
		int tick = (int) RunEnvironment.getInstance().getCurrentSchedule().getTickCount();
		if (this.getSendState() == null) {
			DarwinCore toSend = null;
			if (this.interesting.size() > 0) {
				toSend = this.interesting.firstElement();
				this.interesting.remove(toSend);
			} else if (this.empty.size() > 0) {
				toSend = this.empty.firstElement();
				this.empty.remove(toSend);
			} else if (this.unknown.size() > 0) {
				// only send if processed
				if(this.mode.equals("NKALL") && this.routingMode.equalsIgnoreCase("tiered") || this instanceof SensingNode && this.mode.equals("NKLK")) {
					for(DarwinCore d : this.unknown) {
						if(d.getId() != null) {
							toSend = d;
							this.unknown.remove(toSend);
							break;
						}
					}
				}else{
					toSend = this.unknown.firstElement();
					this.unknown.remove(toSend);
				}
				
			}
			if (toSend != null) {
				this.setSendState(new SendState(toSend, -1));
				this.send();
			}
		} else {
			DarwinCore sending = (DarwinCore) this.getSendState().getFile();
			if (this.getSendState().getTransTime() == -1) {
				double transTime = (sending.getSize() * 8)
						/ this.getTransRate();
				this.getSendState().setTransTime(tick + transTime);
//				System.out.println(sending.getSize() + ": " + transTime);
				this.send();
			} else if (this.getSendState().getTransTime() <= tick) {
				this.getReceiver().receive(sending);
//				System.out.println(this.getReceiver());
				this.setSendState(null);
			}
		}
	}
	
	@ScheduledMethod(start = 10, interval = 1, priority = 1.7976931348623157E308d)
	public void sendDWC() {
		int tick = (int) RunEnvironment.getInstance().getCurrentSchedule().getTickCount();
		if(this instanceof CentralNode)
			return;
		if(this.sendState == null && this.routingMode.equalsIgnoreCase("flat") && this.archives.size() > 0) {
			DarwinCore toSend = this.archives.firstElement();
			this.archives.remove(toSend);
			double transTime = (toSend.getSize() * 8)/ this.getTransRate();
			this.setSendState(new SendState(toSend, tick + transTime));
		}else if(this.getSendState() != null) {
			System.out.println(this.getSendState().getTransTime() + ": " + tick);
			if((int)this.getSendState().getTransTime() == tick) {
				System.out.println("RECVD: " + this.getSendState().getTransTime() + ": " + tick);
				this.getReceiver().receiveDWC((DarwinCore)this.sendState.getFile());
				this.setSendState(null);
			}
		}
	}
	
	public void receiveDWC(DarwinCore recvd) {
		int tick = (int) RunEnvironment.getInstance().getCurrentSchedule()
				.getTickCount();
		Object[] obj = { recvd };
		recvd.addRoute(this);
		switch(this.routingMode) {
			case "tiered":
			break;
			case "flat":
				this.archives.add(recvd);
				if(this instanceof CentralNode) {
					ScheduleParameters sp = null;
					String method = "highKnowledge";
					RunEnvironment inst = RunEnvironment.getInstance();
					sp = ScheduleParameters.createOneTime(inst.getCurrentSchedule()
							.getTickCount(), ScheduleParameters.FIRST_PRIORITY, 43);
					RunEnvironment.getInstance().getCurrentSchedule()
							.schedule(sp, this, method, obj);
				}
			break;
				
		}
	}

	public void receive(DarwinCore recvd) {
		int tick = (int) RunEnvironment.getInstance().getCurrentSchedule()
				.getTickCount();
		recvd.addRoute(this);
		Object[] obj = { recvd };
		if (this instanceof CentralNode && this.routingMode.equals("flat")) {
			this.archives.add(recvd);
//			System.out.println("received");
			ScheduleParameters sp = null;
			String method = "highKnowledge";
			RunEnvironment inst = RunEnvironment.getInstance();
			sp = ScheduleParameters.createOneTime(inst.getCurrentSchedule()
					.getTickCount(), ScheduleParameters.FIRST_PRIORITY, 43);
			RunEnvironment.getInstance().getCurrentSchedule()
					.schedule(sp, this, method, obj);
		}else if(this instanceof CentralNode && this.routingMode.equals("tiered")) {
			this.archives.add(recvd);
			recvd.setEndTime(tick);
			recvd.setDelivered(true);
		} else if (this instanceof SensingNode) {
			this.archives.add(recvd);
			if (recvd.getId() != null) {
				if (recvd.getId().getSpeciesID()
						.equalsIgnoreCase("INTERESTING")) {
//					this.interesting.add(recvd);
					this.setSendState(new SendState(recvd, -1));
				} else if (recvd.getId().getSpeciesID()
						.equalsIgnoreCase("EMPTY")) {
					this.empty.add(recvd);
				}
			} else {
				this.unknown.add(recvd);
			}
		} else if (this instanceof RoutingNode) {
			RunEnvironment inst = RunEnvironment.getInstance();
			ScheduleParameters sp = null;
			String method = "";
			this.archives.add(recvd);
			if (this.mode.equals("HKALL") || this.mode.equals("KHAS")) {
				method = "highKnowledge";
				sp = ScheduleParameters.createOneTime(inst.getCurrentSchedule()
						.getTickCount(), ScheduleParameters.FIRST_PRIORITY, 43);
				RunEnvironment.getInstance().getCurrentSchedule()
						.schedule(sp, this, method, obj);
			} else if (this.mode.equals("LKALL") || this.mode.equals("NKLK")) {
				method = "lowKnowledge";
				sp = ScheduleParameters.createOneTime(inst.getCurrentSchedule()
						.getTickCount(), ScheduleParameters.FIRST_PRIORITY, 5);
				RunEnvironment.getInstance().getCurrentSchedule()
						.schedule(sp, this, method, obj);
			} else {
				this.unknown.add(recvd);
			}
			// System.out.println(this.empty.size());
		}
	}

	public void highKnowledge(DarwinCore received) {
		// Could be implemented to loop through the archives periodically.
		DarwinCore dwc = received;
		Map<Integer, BaseNode> route = received.getRoute();
		Iterator<BaseNode> nodes = route.values().iterator();
		boolean processed = false;
		if (dwc.getId() != null) {
			int nodeId = dwc.getId().getIdentifiedBy();
			while (nodes.hasNext()) {
				BaseNode bn = nodes.next();
				if (bn instanceof RoutingNode && bn.getNodeID() == nodeId
						|| bn instanceof SensingNode && bn.getMode() == "HKALL"
						&& bn.getNodeID() == nodeId) {
					processed = true;
					break;
				}
			}
		}
		// System.out.println(processed);
		if (dwc.getId() == null || dwc.getId() != null && !processed) {
			Calendar cal = Calendar.getInstance();
			if (dwc.getContent().equalsIgnoreCase("INTERESTING")) {
				if (new Random().nextDouble() <= 0.82) {
					dwc.setId(new Identification(dwc.getOcc().getEventID(),
							nodeID, cal.getTime(), "INTERESTING"));
//					this.interesting.add(dwc);
					if(!(this instanceof CentralNode))
						this.setSendState(new SendState(dwc, -1));
				} else {
					dwc.setId(new Identification(dwc.getOcc().getEventID(),
							nodeID, cal.getTime(), "EMPTY"));
					this.empty.add(dwc);
				}
			} else if (dwc.getContent().equalsIgnoreCase("EMPTY")) {
				if (new Random().nextDouble() <= 0.97) {
					dwc.setId(new Identification(dwc.getOcc().getEventID(),
							nodeID, cal.getTime(), "EMPTY"));
					this.empty.add(dwc);
				} else {
					dwc.setId(new Identification(dwc.getOcc().getEventID(),
							nodeID, cal.getTime(), "INTERESTING"));
					this.interesting.add(dwc);
					if(!(this instanceof CentralNode))
						this.setSendState(new SendState(dwc, -1));
				}
			}
		} else {
			if (dwc.getId().getSpeciesID().equalsIgnoreCase("EMPTY"))
				this.empty.add(dwc);
			else if (dwc.getId().getSpeciesID().equalsIgnoreCase("INTERESTING")) {
//				this.interesting.add(dwc);
				if(!(this instanceof CentralNode))
					this.setSendState(new SendState(dwc, -1));
			}else
				this.unknown.add(dwc);
		}
		if(this instanceof CentralNode) {
			int tick = (int) RunEnvironment.getInstance().getCurrentSchedule()
					.getTickCount();
			dwc.setEndTime(tick);
			dwc.setDelivered(true);
		}
	}

	public void lowKnowledge(DarwinCore received) {
		DarwinCore dwc = received;
		if (dwc.getId() == null) {
			Vector<Image> images = dwc.getIs();
			Calendar cal = Calendar.getInstance();
			if (dwc.getContent().equals("INTERESTING")) {
				if (new Random().nextDouble() <= 0.10) {
					dwc.setId(new Identification(dwc.getOcc().getEventID(),
							nodeID, cal.getTime(), "INTERESTING"));
					this.interesting.add(dwc);
					if(!(this instanceof CentralNode))
						this.setSendState(new SendState(dwc, -1));
				} else { 
					if(!(this instanceof CentralNode)) {
						dwc.setId(new Identification(dwc.getOcc().getEventID(),
						 nodeID, cal.getTime(), "UNKNOWN"));
						 this.unknown.add(dwc);
					}
				}
			} else {
				dwc.setId(new Identification(dwc.getOcc().getEventID(),
						 nodeID, cal.getTime(), "UNKNOWN"));
				if(!(this instanceof CentralNode))
					this.unknown.add(dwc);
			}
		} else {
			if (dwc.getId().getSpeciesID().equalsIgnoreCase("EMPTY") && !(this instanceof CentralNode))
				this.empty.add(dwc);
			else if (dwc.getId().getSpeciesID().equalsIgnoreCase("INTERESTING") && !(this instanceof CentralNode))
//				this.interesting.add(dwc);
				this.setSendState(new SendState(dwc, -1));
			else if(!(this instanceof CentralNode))
				this.unknown.add(dwc);
		}
//		System.out.println(received.getContent());
		if(this instanceof CentralNode) {
			int tick = (int) RunEnvironment.getInstance().getCurrentSchedule()
					.getTickCount();
			dwc.setEndTime(tick);
			dwc.setDelivered(true);
		}
	}

	protected BaseNode getReceiver() {
		return receiver;
	}

	protected void setReceiver(BaseNode receiver) {
		this.receiver = receiver;
	}

	public String getProjection() {
		return projection;
	}

	public void setProjection(String projection) {
		this.projection = projection;
	}

	public Vector<DarwinCore> getInteresting() {
		return interesting;
	}

	public void setInteresting(Vector<DarwinCore> interesting) {
		this.interesting = interesting;
	}

	public Vector<DarwinCore> getEmpty() {
		return empty;
	}

	public void setEmpty(Vector<DarwinCore> empty) {
		this.empty = empty;
	}

	public Vector<DarwinCore> getUnknown() {
		return unknown;
	}

	public void setUnknown(Vector<DarwinCore> unknown) {
		this.unknown = unknown;
	}

	public Vector<DarwinCore> getArchives() {
		return this.archives;
	}

	public void setArchives(Vector<DarwinCore> archives) {
		this.archives = archives;
	}

	public ContinuousSpace<Object> getSpace() {
		return space;
	}

	public void setSpace(ContinuousSpace<Object> space) {
		this.space = space;
	}

	public Network<Object> getNetwork() {
		return network;
	}

	public void setNetwork(Network<Object> network) {
		this.network = network;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public String getTransMethod() {
		return transMethod;
	}

	public void setTransMethod(String transMethod) {
		this.transMethod = transMethod;
	}

	public int getNodeID() {
		return nodeID;
	}

	public void setNodeID(int nodeID) {
		this.nodeID = nodeID;
	}

	public Double getTransRate() {
		return transRate;
	}

	public void setTransRate(Double transRate) {
		this.transRate = transRate;
	}

	public Double getTransRange() {
		return transRange;
	}

	public void setTransRange(Double transRange) {
		this.transRange = transRange;
	}

	public int getHopsToDA() {
		return this.hopsToDA;
	}

	public void setHopsToDA(int hopsToDA) {
		this.hopsToDA = hopsToDA;
	}

	public int getConnectedNodes() {
		return this.connectedNodes;
	}

	public void setConnectedNodes(int connectedNodes) {
		this.connectedNodes = connectedNodes;
	}

	public SendState getSendState() {
		return sendState;
	}

	public void setSendState(SendState sendState) {
		this.sendState = sendState;
	}

}
