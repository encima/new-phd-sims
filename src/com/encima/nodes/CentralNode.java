package com.encima.nodes;

import java.util.Iterator;

import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.engine.schedule.ScheduleParameters;
import repast.simphony.parameter.Parameters;
import repast.simphony.query.space.continuous.ContinuousWithin;
import repast.simphony.space.continuous.ContinuousSpace;
import repast.simphony.space.graph.Network;

import com.encima.utils.OutputRecorder;

public class CentralNode extends BaseNode {

	private OutputRecorder recorder;

	public CentralNode(int nodeID, String transMethod,
			ContinuousSpace<Object> space, Network<Object> net, String mode, String tm, OutputRecorder recorder) {
		super(nodeID, transMethod, space, net, mode, tm);

		this.recorder = recorder;
		RunEnvironment inst = RunEnvironment.getInstance();
		ScheduleParameters sp = ScheduleParameters.createOneTime(1);
		RunEnvironment.getInstance().getCurrentSchedule()
				.schedule(sp, this, "linkDPNodes");
	}

	public void linkDPNodes() {
		System.out.println("Initiating Network setup");
		Parameters params = RunEnvironment.getInstance().getParameters();
		int dpNodeCount = (Integer) params.getValue("dpNodeCount");
		String routingMode = (String) params.getValue("routingMode");
		Iterator iter = new ContinuousWithin(this.getSpace(), this, this.getTransRange()).query().iterator();
		int count = 0;
		System.out.println(routingMode.equals("flat"));
		while (iter.hasNext()) {
			Object n = iter.next();
			if (n instanceof RoutingNode && routingMode.equals("tiered")) {
				RoutingNode dp = (RoutingNode) n;
				dp.link(this);
				count++;
			}else if(n instanceof SensingNode && routingMode.equals("flat")) {
				SensingNode dp = (SensingNode) n;
				System.out.println(n.getClass());
				dp.receiveBroadcast(this);
				count++;
			}
		}
		if (count == 0) {
			System.out.println("No neighbouring DP nodes found, ending simulation");
			RunEnvironment.getInstance().endRun();
			this.recorder.deleteFile();
		} else {
			System.out.println("Network Setup Complete, Connected DP Nodes: " + count + " of " + dpNodeCount);
		}
	}
}
