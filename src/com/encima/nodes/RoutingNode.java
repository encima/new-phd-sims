package com.encima.nodes;

import java.util.Iterator;
import java.util.Vector;

import repast.simphony.query.space.continuous.ContinuousWithin;
import repast.simphony.space.continuous.ContinuousSpace;
import repast.simphony.space.graph.Network;
import repast.simphony.space.graph.RepastEdge;

public class RoutingNode extends BaseNode {

	Vector neighbourDCs, neighbourDPs;
	public RoutingNode(int nodeID, String transMethod,
			ContinuousSpace<Object> space, Network<Object> net, String mode, String tm) {
		super(nodeID, transMethod, space, net, mode, tm);
		// TODO Auto-generated constructor stub
		neighbourDCs = new Vector<SensingNode>();
		neighbourDPs = new Vector<RoutingNode>();
	}
	
	public void link(BaseNode req) {
		this.handleEdge(req);
		handleEdge(req);
		this.setReceiver(req);
		System.out.println(req);
		this.setHopsToDA(req.getHopsToDA() + 1);
		Iterator iter = new ContinuousWithin(this.getSpace(), this, this.getTransRange()).query().iterator();
//		Get all DC and DP nodes in transmission range
		while (iter.hasNext()) {
			Object n = iter.next();
			if(n instanceof RoutingNode && !this.neighbourDPs.contains(n)) {
				RoutingNode dp = (RoutingNode) n;
				this.neighbourDPs.add(dp);
			}else if(n instanceof SensingNode && !this.neighbourDCs.contains(n)) {
				this.neighbourDCs.add((SensingNode) n);
			}
		}
		if(this.neighbourDPs.size() > 0) {
			int maxConns = (this.neighbourDCs.size()/this.neighbourDPs.size()) + 1;
			Iterator<SensingNode> dcIter = this.neighbourDCs.iterator();
			System.out.println("Max connections allowed for Node " + this.getNodeID() + ": " + maxConns);
			while(dcIter.hasNext()) {
				SensingNode dc = dcIter.next();
				if(this.calculateConnectedNodes() < maxConns) {
					if(dc.getReceiver() == null || (dc.getReceiver() != null && dc.getReceiver() instanceof SensingNode)) {
						dc.receiveBroadcast(this);
						dcIter.remove();
//						System.out.println(this.getNodeID() + " connecting to: " + dc.getNodeID() + ". Active Connections: " + this.calculateConnectedNodes() + " of " + maxConns);
					}
				}
			}
		}else{
			Iterator<SensingNode> dcIter = this.neighbourDCs.iterator();
			while(dcIter.hasNext()) {
				SensingNode dc  = dcIter.next();
				if(dc.getReceiver() == null || (dc.getReceiver() != null && dc.getReceiver() instanceof SensingNode)) {
					dc.receiveBroadcast(this);
					dcIter.remove();
//					System.out.println(this.getNodeID() + " connecting to: " + dc.getNodeID());
				}
			}
		}
		System.out.println("------");
	}
	
	public int calculateConnectedNodes() {
		this.setConnectedNodes(0);
		int cn = 0;
//		Get edges connected to node
		Iterator<RepastEdge<Object>> itera = this.getNetwork().getEdges(this).iterator();
		while(itera.hasNext()) {
			RepastEdge re = itera.next();
			BaseNode s = (BaseNode) re.getSource();
			BaseNode t = (BaseNode) re.getTarget();
			if(s.equals(this) || t.equals(this)) {
				cn++;
				this.setConnectedNodes(cn);
			}
				
		}
		return this.getConnectedNodes();
	}

}
