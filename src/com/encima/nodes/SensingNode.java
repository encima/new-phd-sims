package com.encima.nodes;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Random;
import java.util.Vector;

import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.engine.schedule.ScheduleParameters;
import repast.simphony.engine.schedule.ScheduledMethod;
import repast.simphony.parameter.Parameters;
import repast.simphony.space.continuous.ContinuousSpace;
import repast.simphony.space.graph.Network;

import com.encima.dwc.DarwinCore;
import com.encima.dwc.Image;
import com.encima.dwc.Occurrence;

public class SensingNode extends BaseNode {

	public SensingNode(int nodeID, String transMethod,
			ContinuousSpace<Object> space, Network<Object> net, String mode, String tm) {
		super(nodeID, transMethod, space, net, mode, tm);
		// TODO Auto-generated constructor stub
	}
	
	
	@ScheduledMethod (start = 10, interval = 1000, priority = 1.7976931348623157E308d)
	public void takeImage() {
		int tick = (int) RunEnvironment.getInstance().getCurrentSchedule().getTickCount();
		Parameters params = RunEnvironment.getInstance().getParameters();
			if(new Random().nextDouble() <= params.getDouble("captureChance")) {
				DarwinCore dwc = genDWCRandom(tick, nodeID);
				//TODO: Add processing time to this
				RunEnvironment inst = RunEnvironment.getInstance();
				ScheduleParameters sp = null;
				Object[] obj = { dwc };
				String method = "";
//				if(this.mode.equals("HKALL")) {
//					method = "highKnowledge";
//					sp = ScheduleParameters.createOneTime(inst
//							.getCurrentSchedule().getTickCount(),
//							ScheduleParameters.FIRST_PRIORITY, 40);
//				}else if(this.mode.equals("LKALL") || this.mode.equals("KHAS")) {
//					method = "lowKnowledge";
//					sp = ScheduleParameters.createOneTime(inst
//							.getCurrentSchedule().getTickCount(),
//							ScheduleParameters.FIRST_PRIORITY, 5);
//				}else{
//					this.unknown.add(dwc);
//				}
//				if(!method.equals("")) {
//					RunEnvironment.getInstance().getCurrentSchedule().schedule(sp, this, method, obj);
//				}

//				this.send();
				this.archives.add(dwc);
//				sendImage();
			}
	}

	public DarwinCore genDWCRandom(int tick, int locID) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
		Calendar cal = Calendar.getInstance();
		dateFormat.format(cal.getTime());
		Occurrence occ = new Occurrence(tick, cal.getTime(), cal.getTime(),
				nodeID, "MOVINGIMAGE", nodeID);
		Vector<Image> isv = new Vector<Image>();
		for (int i = 0; i < 3; i++) {
			isv.add(new Image(i + 1, tick, "path" + i + 1 + ".jpg"));
		}
		// based on a 20% chance of an image containing an object, this is
					// not specific to location but taken across a whole set
		DarwinCore dwc;
		Parameters params = RunEnvironment.getInstance().getParameters();
		if (new Random().nextDouble() <= params.getDouble("interestingChance")) {
//		if (new Random().nextDouble() <= 0.207) {
			dwc = new DarwinCore(occ, null, isv, "INTERESTING");
		}else{
			dwc = new DarwinCore(occ, null, isv, "EMPTY");
		}
		dwc.addRoute(this);
		return dwc;
	}
}
