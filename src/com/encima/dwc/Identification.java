/**
 * 
 */
package com.encima.dwc;

import java.util.Date;

/**
 * @author encima
 * 
 */

public class Identification {

	// @Column(name="id")
	int id;
	// @Column(name="identifiedBy")
	int identifiedBy;
	// @Column(name="dateIdentified")
	Date dateIdentified;
	// @Column(name="speciesID")
	String speciesID;

	public Identification(int id, int identifiedBy, Date dateIdentified,
			String speciesID) {
		this.setId(id);
		this.setIdentifiedBy(identifiedBy);
		this.setDateIdentified(dateIdentified);
		this.setSpeciesID(speciesID);
	}

	public Identification() {

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getIdentifiedBy() {
		return identifiedBy;
	}

	public void setIdentifiedBy(int identifiedBy) {
		this.identifiedBy = identifiedBy;
	}

	public Date getDateIdentified() {
		return dateIdentified;
	}

	public void setDateIdentified(Date dateidentified) {
		this.dateIdentified = dateidentified;
	}

	public String getSpeciesID() {
		return speciesID;
	}

	public void setSpeciesID(String speciesID) {
		this.speciesID = speciesID;
	}
}
