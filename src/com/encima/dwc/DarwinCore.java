package com.encima.dwc;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import com.encima.nodes.BaseNode;

public class DarwinCore {

	Occurrence occ;
	Identification id;
	Vector<Image> is;
	int size = 0;
	String content;
	boolean delivered = false;

	// Repast Details
	public Map<Integer, BaseNode> route = new HashMap<Integer, BaseNode>();
	int endTime = 0;

	public DarwinCore(Occurrence occ, Identification id, Vector<Image> is, String content) {
		super();
		this.occ = occ;
		this.id = id;
		this.is = new Vector<Image>(is);
		calcSize();
		this.endTime = 0;
		this.content = content;
	}

	public Occurrence getOcc() {
		return occ;
	}

	public void setOcc(Occurrence occ) {
		this.occ = occ;
	}

	public Identification getId() {
		return id;
	}

	public void setId(Identification id) {
		this.id = id;
	}

	public Vector<Image> getIs() {
		return is;
	}

	public void setIs(Vector<Image> is) {
		this.is = new Vector<Image>(is);
	}

	public int getSize() {
		calcSize();
		return this.size;
	}

	public void setSize(int size) {
		this.size += size;
	}

	public void calcSize() {
		this.size = 0;
		for (Image img : is) {
			this.size += img.size;
		}
	}

	// Repast Specific Methods

	public Map<Integer, BaseNode> getRoute() {
		return route;
	}

	public void setRoute(Map<Integer, BaseNode> route) {
		this.route = route;
	}

	public void addRoute(BaseNode n) {
		this.route.put(this.route.size() + 1, n);
	}

	public int getTransTime() {
		return endTime;
	}

	public void setEndTime(int transTime) {
		this.endTime = transTime;
	}
	
	public int getEndTime() {
		return this.endTime;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	public boolean isDelivered() {
		return delivered;
	}

	public void setDelivered(boolean delivered) {
		this.delivered = delivered;
	}

}
