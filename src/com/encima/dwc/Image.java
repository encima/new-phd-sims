package com.encima.dwc;

public class Image {

	int id;
	int eventId;
	String identifier;
	int size = 0;

	int minSize = 454;
	int maxSize = 866;

	public Image() {
	}

	public Image(int id, int eventId, String identifier) {
		this.id = id;
		this.eventId = eventId;
		this.identifier = identifier;
		this.size = (minSize + (int) (Math.random() * ((maxSize - minSize) + 1)));
	}

	public Image(int id, int eventId, String identifier, int size) {
		this.id = id;
		this.eventId = eventId;
		this.identifier = identifier;
		this.size = size;
	}

	public Image(int eventId, String identifier) {
		this.eventId = eventId;
		this.identifier = identifier;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getEventId() {
		return id;
	}

	public void setEventId(int id) {
		this.id = id;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

}
