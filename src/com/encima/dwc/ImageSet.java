package com.encima.dwc;

public class ImageSet {

	int id;
	int eventId;
	String identifier;
	int size = 0;

	int minSize = 6970;
	int maxSize = 8660;

	public ImageSet() {
	}

	public ImageSet(int id, int eventId, String identifier) {
		this.id = id;
		this.eventId = eventId;
		this.identifier = identifier;
		this.size = (minSize + (int) (Math.random() * ((maxSize - minSize) + 1)));
	}

	public ImageSet(int eventId, String identifier) {
		this.eventId = eventId;
		this.identifier = identifier;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getEventId() {
		return id;
	}

	public void setEventId(int id) {
		this.id = id;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

}
