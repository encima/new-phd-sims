package com.encima.utils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Vector;

import repast.simphony.context.Context;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.parameter.Parameters;

import com.encima.dwc.DarwinCore;
import com.encima.nodes.BaseNode;
import com.encima.nodes.CentralNode;
import com.encima.nodes.RoutingNode;

public class OutputRecorder {

	Context context;
	SimpleDateFormat sdf = new SimpleDateFormat("ddmmyyyy_HHmmss");
	Path file, undeliveredFile;
	long start = 0;

	public OutputRecorder(Context context, String mode) {
		this.context = context;
		String dt = sdf.format(Calendar.getInstance().getTime());
		Parameters params = RunEnvironment.getInstance().getParameters();
		String routingMode = params.getString("routingMode");
		file = Paths.get("output/" + routingMode + "/" + mode + "/" + mode + "_RUN" + dt + ".csv");
		try {
			Files.createFile(file);
			Files.write(file,
					"DWC_ID, Processed Contents, Actual Contents, Route, Total_Trans_Time, Trans_Time_Minutes, Delivered? \n"
							.getBytes(), StandardOpenOption.APPEND);
			System.out.println("Log File created at " + file.toString());
		} catch (IOException e1) {
			System.err.println(e1);
			System.out.println("Log file creation failed, probably because it already exists");
		}
	}

	public void create() {
		try {
			start = System.currentTimeMillis();
			String startTime = "Start: " + start + "\n";
			Files.write(file, startTime.getBytes(), StandardOpenOption.APPEND);
			System.out.println("Logging Started: " + start);
		} catch (IOException e1) {
			System.out.println("Log file creation failed, probably because it already exists");
		}
	}
	
	public void deleteFile() {
		try {
			Files.delete(file);
		} catch (IOException e) {
			System.err.println("Failed to delete file");
		}
	}

	public void setupTime() {
		if (file != null) {
			long setup = System.currentTimeMillis();
			String setupComp = "Setup Complete: " + (setup) + "\n";
			String setupTime = "Setup Time: " + (setup - start) + "\n";
			try {
				Files.write(file, setupComp.getBytes(),
						StandardOpenOption.APPEND);
				Files.write(file, setupTime.getBytes(),
						StandardOpenOption.APPEND);
				Files.write(file,
						"DWC_ID, Processed Contents, Actual Contents, Route, Total_Trans_Time, Trans_Time_Minutes \n"
								.getBytes(), StandardOpenOption.APPEND);
				System.out.println(setupTime);
			} catch (IOException e) {
				System.out.println("Error appending network setup time");
				e.printStackTrace();
			}
		}
	}

	public void finalise() {
		System.out.println("*****Simulation Ended********");
		Iterator<BaseNode> daNodes = context.getObjects(BaseNode.class).iterator();
		while (daNodes.hasNext()) {
			System.out.println("Looping through nodes");
			BaseNode dan = daNodes.next();
			Vector<DarwinCore> dwc = dan.getArchives();
			if(dwc.size() > 0) {
				for (DarwinCore d : dwc) {
					Iterator route = d.route.values().iterator();
					StringBuilder line = new StringBuilder();
					line.append(d.getOcc().getEventID());
					line.append(", ");
					if (d.getId() != null)
						line.append(d.getId().getSpeciesID());
					line.append(", ");
					line.append(d.getContent() + ", ");
					while (route.hasNext()) {
						BaseNode n = (BaseNode) route.next();
						if (route.hasNext())
							line.append(n.getNodeID() + ":");
						else
							line.append(n.getNodeID() + ", ");
					}
					long dur = d.getEndTime() - d.getOcc().getEventID();
					line.append(dur + ", ");
					line.append((dur / 60) + ", ");
					line.append(d.isDelivered());
					line.append("\n"); 
					try {
						Files.write(file, line.toString().getBytes(), StandardOpenOption.APPEND);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
		System.out.println("Output logged to " + file.toAbsolutePath());
	}

}
