/**
 * 
 */
package simulations;

import java.util.Random;
import java.util.Vector;

import repast.simphony.context.Context;
import repast.simphony.context.DefaultContext;
import repast.simphony.context.space.continuous.ContinuousSpaceFactory;
import repast.simphony.context.space.continuous.ContinuousSpaceFactoryFinder;
import repast.simphony.context.space.graph.NetworkBuilder;
import repast.simphony.dataLoader.ContextBuilder;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.engine.schedule.ISchedule;
import repast.simphony.engine.schedule.ScheduleParameters;
import repast.simphony.parameter.Parameters;
import repast.simphony.space.continuous.ContinuousSpace;
import repast.simphony.space.continuous.NdPoint;
import repast.simphony.space.continuous.RandomCartesianAdder;
import repast.simphony.space.graph.Network;

import com.encima.dwc.DarwinCore;
import com.encima.nodes.CentralNode;
import com.encima.nodes.RoutingNode;
import com.encima.nodes.SensingNode;
import com.encima.utils.OutputRecorder;

/**
 * @author encima
 * 
 */
public class SensorNetBuilder extends DefaultContext<Object> implements
		ContextBuilder<Object> {
	ContinuousSpace<Object> space;

	public Context<Object> build(Context<Object> context) {

		context.setId("K-HAS");

		// Geography<Object> geography =
		// GeographyFactoryFinder.createGeographyFactory(null)
		// .createGeography("Geography", context, new
		// GeographyParameters<Object>());

		NetworkBuilder<Object> netbuild = new NetworkBuilder<Object>(
				"sensor network", context, true);
		Network<Object> net = netbuild.buildNetwork();

		Parameters params = RunEnvironment.getInstance().getParameters();
		int height = (Integer) params.getValue("height");
		int width = (Integer) params.getValue("width");
		int daNodeCount = (Integer) params.getValue("daNodeCount");
		int dpNodeCount = (Integer) params.getValue("dpNodeCount");
		int dcNodeCount = (Integer) params.getValue("dcNodeCount");
		int dcHK = (Integer) params.getValue("dcHK");
		String mode = (String) params.getValue("mode");
		int csv = (Integer) params.getValue("csv");
		int tickStop = (Integer) params.getValue("tickStop");
		
		ContinuousSpaceFactory spaceFactory = ContinuousSpaceFactoryFinder
				.createContinuousSpaceFactory(null);
		space = spaceFactory.createContinuousSpace("space", context,
				new RandomCartesianAdder<Object>(),
				new repast.simphony.space.continuous.StrictBorders(), width,
				height);
		// Initialise log file and write start time
		OutputRecorder recorder = new OutputRecorder(context, mode);

		int i = 0;
		Vector<DarwinCore> dwcs = null;
		if(mode.equals("KHAS") && dcHK != -1) {
			for(; i < dcHK; i++) {
				System.out.println("*Adding HK DC node");
				SensingNode n = new SensingNode(i, "rf", space, net, "HKALL", "rf");
				context.add(n);
//				dcNodeCount--;
			}
		}
		
		CentralNode can = null;
		for(; i < daNodeCount; i++) {
			can = new CentralNode(i, "wi-fi", space, net, mode, "wifi", recorder);
			context.add(can);
		}
		NdPoint canLoc = space.getLocation(can);
		double tr = can.getTransRange();
		double minX = canLoc.getX() - tr;
		double minY = canLoc.getY() - tr;
		double maxX = canLoc.getX() + tr;
		double maxY = canLoc.getY() + tr;
		System.out.println(canLoc.toString());
		for(; i < daNodeCount + dpNodeCount; i++) {
			RoutingNode dpn = new RoutingNode(i, "rf", space, net, mode, "wifi");
			context.add(dpn);
			Random r = new Random();
			double randX = minX + (maxX - minX) * r.nextDouble();
			double randY = minY + (maxY - minY) * r.nextDouble();
			if(randX > space.getDimensions().getWidth() || randX < 0) {
				randX = canLoc.getX() + 5;
			}
			if(randY > space.getDimensions().getHeight() || randY < 0) {
				randY = canLoc.getY() + 5;
			}
			System.out.println("X: " + randX + ", Y: " + randY);
			double[] dpnLoc = new double[]{randX, randY};
			space.moveTo(dpn, dpnLoc);
		}

		for (; i < daNodeCount + dcNodeCount + dpNodeCount; i++) {
//			if (csv == 1)
//				dwcs = FileUtils.readArchives(images, i);
//				System.out.println("Adding DC node");
				SensingNode n = new SensingNode(i, "rf", space, net, mode, "rf");
				context.add(n);
		}

		for (; i < dpNodeCount + dcNodeCount; i++) {
			RoutingNode dpn = new RoutingNode(i, "rf", space, net, mode, "wifi");
			context.add(dpn);
		}

		for (; i < daNodeCount + dcNodeCount + dpNodeCount; i++) {
			CentralNode dan = new CentralNode(i, "wi-fi", space, net, mode, "wifi", recorder);
			context.add(dan);
		}
		
		if(tickStop > -1) {
//			End after 6 month deployment
			RunEnvironment.getInstance().endAt(tickStop);
		}

		// Schedule custom output to log file at the end of the simulation
		ISchedule schedule = RunEnvironment.getInstance().getCurrentSchedule();
		ScheduleParameters schedParams = ScheduleParameters.createAtEnd(0);
		schedule.schedule(schedParams, recorder, "finalise");

		System.out.println("***********Initialised Network***********");
		return context;
	}

}
