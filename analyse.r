# csvdata<-read.csv(file.choose(), header=TRUE, stringsAsFactors=FALSE)
#setwd("/Users/encima/Development/java/repast/phd-simulations/output/NKALL")

setwd("C:/Users/encima/Dev/phd-simulations/output/tiered/KHAS")

files <- list.files(pattern = "\\.(csv|CSV)$")
csvdata<-read.csv(files[1], header = TRUE, stringsAsFactors=FALSE) 
#for(i in 2:length(files)) {
#	temp <- read.csv(files[i], header = TRUE, stringsAsFactors=FALSE) 
#	csvdata<-rbind(csvdata, temp)
#} 
print(length(files))
#by(csvdata, 1:nrow(csvdata), function(row) print(length(strsplit(row$Route, ":")[[1]])))
# create truepositive, falsepositive, true negative and falsenegative data frames
int_data<-subset(csvdata, csvdata$Actual.Contents==" INTERESTING")
tp_data<-subset(int_data, int_data$Processed.Contents==" INTERESTING")
fn_data<-subset(int_data, int_data$Processed.Contents==" EMPTY")
null_data<-subset(int_data, is.na(int_data$Processed.Contents))
fn_data<-rbind(fn_data, null_data)
if(nrow(fn_data) == 0) {
  print('fn_data is empty, filling...')
  fn_data<-subset(int_data, !int_data$Processed.Contents==" INTERESTING")
}

empty_data<-subset(csvdata, csvdata$Actual.Contents==" EMPTY")
empty_null_data<-subset(empty_data, is.na(empty_data$Processed.Contents))
empty_null_data<-rbind(empty_null_data, subset(empty_data, empty_data$Processed.Contents==" "))
tn_data<-subset(empty_data, empty_data$Processed.Contents==" EMPTY")
tn_data<-rbind(tn_data, empty_null_data)
fp_data<-subset(empty_data, empty_data$Processed.Contents==" INTERESTING")

getStats <- function(dataset) {
  data_mean <- mean(dataset$Total_Trans_Time)
  data_median <- median(dataset$Total_Trans_Time)
  data_sd <- sd(dataset$Total_Trans_Time)
  print(length(strsplit(dataset$Route, ":")))
  data_route <- 0
  if(length(strsplit(dataset$Route, ":")) > 0) {
    data_route <- mean(length(strsplit(dataset$Route, ":")[[1]]))
  }
  data_se <- (sd(dataset$Total_Trans_Time)/(sqrt(length(dataset$Total_Trans_Time))))
    #length(files))))
  return(list(mean=data_mean, median=data_median, sd=data_sd, se=data_se, mean_hops=data_route))
}

print(getwd())
print('STATS')
print('TOTAL:')
total_stats <- getStats(csvdata)
print(total_stats)
print('*******')
print('INTERESTING:')
int_stats <- getStats(int_data)
int_stats <- c(int_stats, percent=((nrow(int_data)/nrow(csvdata))*100))
print(int_stats)
print('*******')
print('*TP:')
tp_stats <- getStats(tp_data)
tp_stats <- c(tp_stats, percent=((nrow(tp_data)/nrow(csvdata))*100))
print(tp_stats)
print('*******')
print('*FN:')
fn_stats <- getStats(fn_data)
fn_stats <-c(fn_stats, percent=((nrow(fn_data)/nrow(csvdata))*100))
print(fn_stats)
print('*******')
print('EMPTY')
empty_stats <- getStats(empty_data)
empty_stats <- c(empty_stats, percent=((nrow(empty_data)/nrow(csvdata))*100))
print(empty_stats)
print('*******')
print('TN')
tn_stats <- getStats(tn_data)
tn_stats <- c(tn_stats, percent=((nrow(tn_data)/nrow(csvdata))*100))
print(tn_stats)
print('*******')
print('FP')
fp_stats <- getStats(fp_data)
fp_stats <- c(fp_stats, percent=((nrow(fp_data)/nrow(csvdata))*100))
print(fp_stats)

df <- data.frame(total=total_stats, int=int_stats, tp=tp_stats, fn=fn_stats, empty=empty_stats, tn=tn_stats, fp=fp_stats)
write.csv(df, file='stats.txt')
print('output saved')