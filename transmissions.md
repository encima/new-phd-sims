Average size in kilobits: 880KB is 7040kb
Wifi Bit Rate: 55296kbps
Zigbee Bit Rate: 250kbps
---
Best Case (1 DC and 1 DP)
DC: 7040 / 250 = 28.16 seconds
DP: 7040 / 55296 = 0.127 seconds
Total: 28.287 seconds per image = 86.61 per archive
---
Worst Case (4 DC and 1 DP)
DC: 28.16 * 4 = 104.64
DP: 0.127
---
Images marked as null are still part of interesting or empty

Change average depth and run like a corridor

Plot empty percent

Vary transmission rates or try with much lower rates - random number for each link at the start of the run. Showing how localized variables affect the rate. Do not affect zigbee transmission

Hobble the network with higher congestion.