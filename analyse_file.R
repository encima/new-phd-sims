# csvdata<-read.csv(file.choose(), header=TRUE, stringsAsFactors=FALSE)
#setwd("/Users/encima/Development/java/repast/phd-simulations/output/NKALL")

setwd("/Users/encima/Development/java/repast/phd-simulations/output/LKALL")

getStats <- function(dataset) {
  data_mean <- mean(dataset$Total_Trans_Time, na.rm=TRUE)
  data_median <- median(dataset$Total_Trans_Time)
  data_sd <- sd(dataset$Total_Trans_Time, na.rm=TRUE)
  #print(length(strsplit(dataset$Route, ":")))
  #data_route <- 0
  #if(length(strsplit(dataset$Route, ":")) > 0) {
  #  data_route <- mean(length(strsplit(dataset$Route, ":")[[1]]))
  #}
  #data_se <- (sd(dataset$Total_Trans_Time)/(sqrt(length(dataset$Total_Trans_Time))))
  #length(files))))
  return(list(mean=data_mean, median=data_median, sd=data_sd))
}

int_mean <- NULL
int_sd <- NULL
tp_mean <- NULL
tn_mean <- NULL
empty_mean <- NULL
fp_mean <- NULL
fn_mean <- NULL

files <- list.files(pattern = "\\.(csv|CSV)$")
csvdata<-read.csv(files[1], header = TRUE, stringsAsFactors=FALSE) 
for(i in 2:length(files)) {
  temp <- read.csv(files[i], header = TRUE, stringsAsFactors=FALSE) 
  int_data<-subset(temp, csvdata$Actual.Contents==" INTERESTING")
  int_stats = getStats(int_data)
  int_mean <- c(int_mean, int_stats$mean)
  int_se <-c(int_sd, int_stats$sd)
  tp_data<-subset(int_data, int_data$Processed.Contents==" INTERESTING")
  fn_data<-subset(int_data, int_data$Processed.Contents==" EMPTY")
  null_data<-subset(int_data, is.na(int_data$Processed.Contents))
  fn_data<-rbind(fn_data, null_data)
  if(nrow(fn_data) == 0) {
    #print('fn_data is empty, filling...')
    fn_data<-subset(int_data, !int_data$Processed.Contents==" INTERESTING")
  }
  empty_data<-subset(temp, csvdata$Actual.Contents==" EMPTY")
  empty_null_data<-subset(empty_data, is.na(empty_data$Processed.Contents))
  empty_null_data<-rbind(empty_null_data, subset(empty_data, empty_data$Processed.Contents==" "))
  tn_data<-subset(empty_data, empty_data$Processed.Contents==" EMPTY")
  tn_data<-rbind(tn_data, empty_null_data)
  fp_data<-subset(empty_data, empty_data$Processed.Contents==" INTERESTING")
}

print(mean(int_mean), na.rm=TRUE)
print(sd(int_mean), na.rm=TRUE)
print(sd(int_mean)/sqrt(length(files)))

#df <- data.frame(total=total_stats, int=int_stats, tp=tp_stats, fn=fn_stats, empty=empty_stats, tn=tn_stats, fp=fp_stats)
#write.csv(df, file='stats.txt')
#print('output saved')